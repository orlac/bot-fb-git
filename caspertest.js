var async = require('async');
var casper = require('casper').create({
	verbose: true,
	// logLevel: 'error',
	pageSettings: {
		loadImages: true,
		loadPlugins: true,
		userAgent: 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0'
	},
	viewportSize: {
		width: 1366,
		height: 768
	}
});

var users = {
	email: '', 
	pass: ''
}

// casper.cli.options["engine"] = "slimerjs";
// casper.cli.options["proxy"] = "61.90.71.127:8888";
// casper.cli.options["proxy-type"] = "http";

casper.start('http://facebook.com');

casper.waitForSelector('#login_form', function(){
	this.fill('form#login_form', users, true);
	this.wait(3000);
});



casper.then( function() {
    this.click('[type="submit"]');
});

casper.waitForSelector('[title="Профиль"]', function(){
	this.click('[title="Профиль"]');
});

casper.waitForSelector('.uiLinkButtonInput', function(){
	//this.click('.uiLinkButtonInput');
	this.scrollToBottom();
	this.wait(3000);
});

// casper.start('http://facebook.com', function() {
// 	this.fill('form#login_form', {
//         'email':    users.login,
//         'password':    users.pass
//     }, true);
//     this.click('[type="submit"]');
// 	console.log('open');
// 	// this.viewport(1024, 768);
// 	this.wait(1000);
//     this.echo(this.getTitle());
// });

casper.then( function() {
    this.echo('complete');
    this.wait(10000);
});

casper.run();