
var config = require('./config/config');
var db = require('./app/models');
var spawn = require('child_process').spawn;
var async = require('async');
var SpookyService = require('./app/services/spookyService');
var webdriverio = require('webdriverio');
// var selenium = require('selenium-standalone');
// var logger = require('./app/logger');
// logger.log(config);
// logger.logger. debug(config);

 
db.sequelize
  .sync()
  .then(function () {
  	
  	// var _sp = new spService().start('http://en.wikipedia.org/wiki/Spooky_the_Tuff_Little_Ghost');
    //io.set('resource', config.socketResource);
    
    //io.serverClient(false);
    //io.set('authorization', app.user.auth);
    
    var userService = require('./app/services/userService')(db, config);
    console.log('config.root', config.root);

    var _start = function(){
      userService.getUsers().then(function(users){
        async.each( users, function(user, next){
            
            var options = {
              desiredCapabilities: {
                //browserName: 'chrome',
              },
            };

            var spookyService = new SpookyService(options, user, db, config);
            spookyService.start();

        }, function(){
            console.log('complete');
        });
      }).catch(function(e){
          throw new Error(e);
      });
    }

    // selenium.start(function(err, child) {
    //   if(err) throw err;
    //   _start();
    // });
    _start();
    
    
    
  }).catch(function (e) {
    throw new Error(e);
  });


