var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config_local = require('./config-local.js');
var _ = require('lodash');

var config = {
  development: {
    env: env,
    root: rootPath,
    browserName: 'phantomjs',
    target: {
      home: 'https://m.facebook.com'
    },
    viewportSize: {
      width: 1024,
      height: 600
    },
    
    user: 'root',
    password: '123',
    db: 'mysql://root:123@localhost/fb_bot',

    screenPath: process.cwd()+'/screen'

  },

  test: {
    root: rootPath,
    db: 'mysql://localhost/fb_bot_test'
  },

  production: {
    root: rootPath,
    db: 'mysql://localhost/fb_bot_production'
  }
};

config = _.merge(config, config_local);

module.exports = config[env];
