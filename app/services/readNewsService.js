'use strict';

var async = require('async');
var randomService = require('./randomService');
var clickNewsService = require('./clickNewsService');
var ScreenService = require('./screenService');

var selectors = {
	linkTofeed: '.mmi_feed a',	
	linkToMore: '.show_more_wrap a',
}

module.exports = function(client, user, db, config){

	var mainUrl = config.target.home;
	var screenService = ScreenService(client, user, db, config);

	var _is_offset_footer = function(callBack){
		client
			.timeoutsAsyncScript(10000)
			.executeAsync(function(a, done){
			done(document.body.scrollHeight);

		}, true, function(err, pos){
			console.log('position footer', err, pos.value);
			callBack(err, pos.value);
		});
	}

	var _native_scroll = function(height, callBack){
		client
		.timeoutsAsyncScript(10000)
		.executeAsync(function(_height, done){
			
			var _pos = 0;
			var _steps = 10
			var _step = _height/_steps;
			var _tm = 200;
			var i = 0;

			var _start = document.body.scrollTop;

			var _next = function(){
				// document.body.scrollTop = document.body.scrollTop + _height;
				// setInterval(function(){
				// 	done( { scrollHeight: document.body.scrollHeight, scrollTop: document.body.scrollTop } );
				// }, _tm);
				i++;
				if( i >= _steps ||  document.body.scrollTop >=  _start + _height){
					done( { scrollHeight: document.body.scrollHeight, scrollTop: document.body.scrollTop } );		
				}else{
					document.body.scrollTop = document.body.scrollTop + _step;
					setInterval(_next, _tm)
				}

				// if( i >= _steps (document.body.scrollTop <  _start + _height) ){
				// 	// document.body.scrollTop = document.body.scrollTop + _step;
				// 	document.body.scrollTop = document.body.scrollTop + _height;
				// 	setInterval(_next, _tm)
				// }else{
				// 	done( { scrollHeight: document.body.scrollHeight, scrollTop: document.body.scrollTop } );		
				// }	
			}
			_next();

		}, height, function(err, pos){
			pos = pos || {};
			console.log('position footer', err, pos.value);
			callBack(err, pos.value);
			//если конец, кликнуть дальше
			// if(pos.value.scrollHeight  <  pos.value.scrollTop + 500 ){
			// 	client.click(selectors.linkToMore, function(err){
			// 		console.log('click more...', err);
			// 		callBack(err, pos.value);	
			// 	});
			// }else{
			// 	callBack(err, pos.value);	
			// }
			

		});
	}

	var _scroll_task = function(i){
		
		return function( end, next){
			console.log('i', i);
			//если промотали до конца
			if(i> 0 && end) return next(null, end);
			if(i == 0){
				next = end;
			}

			var _h = config.viewportSize.height/1;// + config.viewportSize.height/4;
			console.log('_h', _h);
			_native_scroll( _h, function(err, res){
			//client.scroll(0, _h, function(err, res){
				console.log('on scroll', err);
				randomService.run(user, randomService.actions.READ_NEWS).then(function(ms){
					console.log('wait...'+ms);
					client
						.pause(ms*1000 )
						.call(function(){
							
							// screenService.save('scroll_'+i);
							screenService.save('scroll_');
							next(err, false);

							// _is_offset_footer(function(err, clientHeight){
							// 	console.log('isVisible...', err, clientHeight);
							// 	console.log('on wait...');
							// 	var isVisible = clientHeight  <  ( _h  + config.viewportSize.height )
							// 	if(isVisible){
							// 		console.log('scroll end');
							// 		client
							// 			.pause(10*1000).call(function(){
							// 				next(err, true);			
							// 			});
							// 	}else{
							// 		next(err, false);				
							// 	}
							// });

							// client.element('._5h6_', function(err, isVisible){
							// 	console.log('isVisible...', err, isVisible);
							// 	console.log('on wait...');
							// 	if(isVisible && isVisible.status){
							// 		client
							// 			.pause(10*1000).call(function(){
							// 				next(err, isVisible.status);			
							// 			});
							// 	}else{
							// 		next(err, false);				
							// 	}
							// });
							
						});
					// client.timeoutsImplicitWait(ms*1000, function(){
					// 	console.log('on wait...');
					// 	next(err);		
					// });
				});
				
			});	
		}
		
	}

	var _scroll_all = function(tasks){
		return new Promise(function (resolve, reject) {
			async.waterfall(tasks, function(err){
				if(err) return reject(err);
				console.log('read all');

				// прокликать ссылки в постах
				// TODO делать это случайно и во время прокрутки
				
				(new clickNewsService(client, user, db, config)).run().then(resolve).catch(reject);

				//_click_links().then(resolve).catch(reject);

				//return resolve();
			});			
		});
	}

	var _profile = function(){
		return new Promise(function(resolve, reject){
			resolve();
			// client.click('[title="Profile"]', function(err){
			// 	if(err) return reject(err);
			// 	console.log('logined');
			// 	screenService.save('profile');
			// 	resolve();
			// });	
		})
		
	}

	var _start = function(){
		// рандомное количество промоток вниз
		// рандомное время остановки после промотки
		// 
		return _profile().then(function(){
				return randomService.run(user, randomService.actions.COUNT_READ_NEWS).then(function(count){
				count = Math.max( 4, count ) * 5;

				console.log('count scrolls: ', count);
				return new Promise(function (resolve, reject) {

					client.click('[data-sigil="popover feed"] a', function(err){
						
						console.log('start read...', err);
						if(err) return reject(err);

						client.pause(4000, function(){
								
							var i = 0;
							var tasks = [];
							while(i <= count){
								tasks.push(_scroll_task(i));
								i++;
							}
							_scroll_all(tasks).then(resolve).catch(reject);

						});	
					});
					
				});	
			});	
		})
		
	}

	var run = function(){
		return _start();
		// console.log('start read news');
		// var jqh = new jQueryHelper(client); 
		// return  jqh.run().then(_start);
	}

	return {
		run: run
	}

}