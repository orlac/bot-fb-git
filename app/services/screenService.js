'use strict';

var fs = require('fs');
var rmdir = require( 'rmdir' );

module.exports = function(client, user, db, config){


	var _fn = function(){


		var write = function(action, cb){
			client.saveScreenshot(config.screenPath+'/'+user.id+'/'+action+'.png', cb);
		}

		var write2 = function(action, cb){


			client.takeScreenshot().then(function(data) {
			  	
			  	fs.writeFile(config.screenPath+'/'+user.id+'/'+action+'.png', data, 'base64', cb);
			}, function(err){
				console.log('screen', err);
				cb(err);
			});
		}

		this.clear = function(cb){
			var path = config.screenPath+'/'+user.id;
			rmdir(path, function(err){
				console.log('screen clear', err);
			});
		}

		this.save2 = function(action, cb){
			var path = config.screenPath+'/'+user.id;
			fs.readdir(path, function(err, files){
				if(err){
					fs.mkdir(path, function(err){
						if(err){
							throw err;
						}
						write2(action, cb);
					})
				}else{
					write2(action, cb);
				}
			})
			
		}

		this.save = function(action, cb){
			var path = config.screenPath+'/'+user.id;
			fs.readdir(path, function(err, files){
				if(err){
					fs.mkdir(path, function(err){
						if(err){
							throw err;
						}
						write(action, cb);
					})
				}else{
					write(action, cb);
				}
			})
			
		}
	}

	return new _fn();

}