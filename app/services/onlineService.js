'use strict';

/**
 * Юзер просто находится в сетиы
 */


var config = require('../../config/config.js');  
var casper = null;
var user = null;

var randomService = require('randomService');

//todo в настройки
var mainUrl = config.target.home;

var run = function(url){
	url = url || mainUrl;

	casper.thenOpen(url, function() {
		var self = this;
	    self.echo('i here...');
		randomService.run(user, randomService.actions.ONLINE).then(function(sec){
			self.echo('wait ... '+sec);
			self.wait(sec*1000);
		});	    
	    
	});
}

module.exports = {
	run: run
}