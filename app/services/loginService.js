'use strict';

/**
 * Первый заход
 */
var Spooky = require('spooky');
var Promise = require('promise');
var _ = require('lodash');
var ScreenService = require('./screenService');

var formSelector = '.mobile-login-form'
// var formSelector = '#login_form'

var selectors = {
	form: '.mobile-login-form',
	// form: '#login_form',
	
	button: '#u_0_1',
	// button: '#loginbutton',
	// 
	logined: '[data-sigil="popover feed"] a',
	// logined: '[title="Profile"]'
	// 
	email: '[name="email"]',
	pass: '[name="pass"]',
}

module.exports = {

	

	login: function(client, user, db, config){

		var screenService = ScreenService(client, user, db, config);

		var mainUrl = config.target.home;
		console.log('mainUrl', mainUrl);
		var userService = require('./userService')(db, config);
		
		var _login = function(){
			
			return new Promise(function (resolve, reject) {
				
				var _submit = function(){
					client
						.timeoutsAsyncScript(5000)
						.executeAsync(function(_form, done){
							
							document.getElementsByClassName('mobile-login-form')[0].submit()
							done();

						}, selectors.form, function(err){
							console.log('submit', err);
							screenService.save('send-login');
							_next(err);

					});
				}

				var _next = function(err){


					// client.click(selectors.button, function(){
					// client.submitForm(selectors.form, function(err){
					// client.keys('Enter', function(){
						console.log('submit login', err);
						console.log('wait page load');
						client.timeouts('page load', 3000, function(){
							console.log('page loaded');
							screenService.save('send-login');
							client.waitFor(selectors.logined, 10000, function(err, res){
								screenService.save('enter-login');
								client.click(selectors.logined, function(err){
									if(err) return reject(err);
									console.log('logined');
									screenService.save('profile');
									client.getCookie(function(err, cookies) {
										//console.log('cookies', cookies); 
										user.setUserCookies(cookies);
										user.save().then(resolve).catch(reject);
										// resolve();	
									});
								})
								// if(err) return reject(err);
								// console.log('logined');
								// client.getCookie(function(err, cookies) {
								// 	//console.log('cookies', cookies); 
								// 	user.setUserCookies(cookies);
								// 	user.save().then(resolve).catch(reject);
								// 	// resolve();	
								// });
							});	
						});
					// });
				}

				client.setValue(selectors.email, user.email, function(err){
					console.log('fill email', err);
					client.setValue(selectors.pass, user.password, function(err){
						console.log('fill pass', err);
						screenService.save('fill-login', function(err){
							console.log('fill-login', err);
							_submit();		
						});	
					});	
				});
				
			});
		}

		
		

		return new Promise(function (resolve, reject) {
			var uCookies = user.getUserCookies();
			console.log('\n\nstart\n\n');



			client.url(mainUrl, function(err, res){
				if(err) return reject(err);

				console.log('\n\non start\n\n');				

				if(config.browserName != 'phantomjs' || true){
					if(uCookies){
						console.log('client already logined...');
						// uCookies = uCookies.slice(0, 1);
						_.each(uCookies, function(c){
							client.setCookie(c);	
						});
					}
					client.refresh( function(err, res){
						if(err) return reject(err);

						return client./*pause(10*1000).*/element(selectors.form, function(err, res){
							screenService.save('login');
							if(err) return resolve();
							
							client.url(mainUrl, function(){
								client./*pause(10*1000).*/element(selectors.form, function(err, res){
									screenService.save('re-login');
									if(err) return resolve();	
									_login().then(resolve).catch(reject);
								})
							});
							
						});	
					});
				}else{
					return client./*pause(10*1000).*/element(selectors.form, function(err, res){
							screenService.save('login');
						if(err) return resolve();
						_login().then(resolve).catch(reject);
						
					});
				}

				

				
				
			});
			
		});
	}

}


// var _fn = function(casper, user, db, config){

// 	this.db = db;
// 	this.config = config;
// 	this.user = user;
// 	this.casper = casper;

// 	var mainUrl = config.target.home;

// 	/*var setCookie = function(){
// 		var cookie = user.cookie;
// 		var domain = "facebook.com";
// 		cookie.split(";").forEach(function(pair){
// 		    pair = pair.split("=");
// 		    phantom.addCookie({
// 		      'name': pair[0],
// 		      'value': pair[1],
// 		      'domain': domain
// 		    });
// 		});
// 	}*/

// 	// this.init  = function(){
		
// 	// 	var casper = require('casper').create({
// 	// 		verbose: true,
// 	// 		// logLevel: 'error',
// 	// 		pageSettings: {
// 	// 			loadImages: true,
// 	// 			loadPlugins: true,
// 	// 			userAgent: user.useragent
// 	// 		},
// 	// 		viewportSize: {
// 	// 			width: 1366,
// 	// 			height: 768
// 	// 		}
// 	// 	});
// 	// 	this.casper = casper;
// 	// }

// 	this.login = function(){
// 		// return new Promise(function (resolve, reject) {
// 		// 	resolve(chance.second());
// 		// });
// 		console.log('mainUrl', mainUrl);
// 		casper.start(mainUrl);
// 		var _fn = function(){
			
// 			this.emit('dbg', 'start login');

// 			if (this.exists('#login_form')) {
// 				this.emit('dbg', 'login_form...');
// 		        this.fill('form#login_form', {
// 		        	email: user.email,
// 		        	pass: user.password,
// 		        }, true);
// 				// this.wait(3000);

// 				this.click('[type="submit"]');

// 				casper.waitForSelector('[title="Профиль"]', function(){
// 					this.echo ('logined')
// 					this.emit('dbg', 'logined...');
// 				});

// 		    }else{
// 		    	this.echo ('already logined')
// 		    	this.emit('dbg', 'already logined...');
// 		    }
// 		}
// 		casper.then([{user: user}, _fn]);
// 	}

// 	//this.init();

// }


// module.exports = function(casper, user, db, config){

// 	return new _fn(casper, user, db, config);

// }