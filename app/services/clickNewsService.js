'use strict';

var async = require('async');
var randomService = require('./randomService');
var ScreenService = require('./screenService');

module.exports = function(client, user, db, config){

	var screenService = ScreenService(client, user, db, config);

	var _click_link = function(){
		
		return function(next){

			var pause = ( Math.random()*10 ).toFixed(0);

			/*client.selectorExecute("a._5msj", function(as) {
				// var message = 'There are ';
				// message += divs.length + ' divs on the page';
				// message += ' and ';
				// message += links.length + ' links with an link text "' + links[0].text + '"';
				var res = [];
				for(var i in as){
					res.push(as.href);
				}
				return res;
			}, function(err, arr){
				console.log('links', err, arr);
			});*/

			client
				.pause(pause*1000)
				.elements('a._5pcq', function(err, elements){

					if(err) return next(err);
					console.log('click to elements', err, elements.value.length);		
					

					randomService.getRandomLink(elements.value, user, randomService.actions.COUNT_LINKS_NEWS ).then(function(element){

						if(!element) return next('no elements to click');
						client.elementIdAttribute(element.ELEMENT, 'href', function(err, href){
							
							console.log('href:', err, href);	
							if(err) return next();

							client.elementIdClick(element.ELEMENT, function(err, res){
								console.log('on click', err);
								//если ошибка, вывести в консоль и продлжить
								// if(err) return next(serr);
								if(err) return next();

								
								
								console.log('view post ', pause, 'sec');
								client
									.pause(pause*1000)
									.call(function(){
										screenService.save('post-'+element.ELEMENT);		
										client.back(function(err){
											console.log('on back', err);
											if(err) return next(err);
											client.pause(pause).call(next);

										});
									});
									
							});
						});

						

					}).catch(next);
				});

		}

	}

	var _click_all = function(tasks){
		return new Promise(function (resolve, reject) {
			async.waterfall(tasks, function(err){
				console.log('click all', err);
				if(err) return reject(err);
				resolve();
			});			
		});
	}

	var _click_links = function(){
		return new Promise(function (resolve, reject) {

			client.elements('a._5pcq', function(err, elements){
				if(err) return reject(err);
				console.log('elements', err, elements.value.length);

				randomService.getCountLinks(elements.value, user, randomService.actions.COUNT_LINKS_NEWS ).then(function(count){

					console.log('links to view: ', count);
					var i = 0;
					var tasks = [];
					while(i <= count){
						tasks.push(_click_link());
						i++;
					}

					// var tasks = [];
					// for(var i in els){
					// 	tasks.push(_click_link(els[i]));						
					// }

					_click_all(tasks).then(resolve).catch(reject);


				}).catch(reject);

				// client.elementIdClick(elements.value[0].ELEMENT, function(err, res){
				// 	console.log('on click', err, res);
				// 	if(err) return reject(err);
				// 	return resolve();
				// });

				
			});

		});
	}
	

	var run = function(){
		return _click_links();
		
	}

	return {
		run: run
	}

}