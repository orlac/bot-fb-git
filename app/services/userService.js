'use strict';

/**
 * Берутся юзеры, которые не онлайн и с которыми что-то нужно сделать
 */
var _fn = function(db, config){

	this.db = db;
	this.config = config;

}

_fn.prototype.getUsers = function() {
	return this.db.user.findAll({ where: {
			is_online: false
		} });
};

module.exports = function(db, config){

	return new _fn(db, config);

}