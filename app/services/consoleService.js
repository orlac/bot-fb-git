

module.exports = {

	log: function(_spoky){

		_spoky.emit('_console', args.slice(1));
	},

	start: function(spooky){

		spooky.on('dbg', function (e, stack) {
            console.log(e);

            // if (stack) {
            //     console.log(stack);
            // }
        });

        spooky.on('error', function (e, stack) {
            console.error(e);

            if (stack) {
                console.log(stack);
            }
        });

	}

}