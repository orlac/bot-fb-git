var _ = require('lodash');
var LoginService = require('./loginService');
var readNewsService = require('./readNewsService');
var ScreenService = require('./screenService');

var webdriverio = require('webdriverio');



var fn = function(opts, user, db, config){

    // var _ua = user.useragent;
	//планшет
    // var _ua = "Mozilla/5.0 (Linux; Android 4.3; Nexus 7 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36";
    // var _ua = "Mozilla/5.0 (Linux; U; Android 2.2; en-us; SCH-I800 Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
    // var _ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.34 (KHTML, like Gecko) Chrome/13";
    // var _ua = "Mozilla/5.0 (Linux; U; Android 4.2.2; ru-ru; PMP707 Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30";
    // var _ua = "Mozilla/5.0 (Android; Tablet; rv:38.0) Gecko/38.0 Firefox/38.0";
    // var _ua = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5";
    // var _ua = "Mozilla/5.0 (Linux; ru-ru; PMP707 Build/JDQ39) AppleWebKit/533.30 (KHTML, like Gecko) Version/4.0 Safari/533.30";
    var _ua = "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.634.0 Safari/534.16";
    // var _ua = "Mozilla/5.0 (X11; Linux amd64) AppleWebKit/534.36 (KHTML, like Gecko) Chrome/13.0.766.0 Safari/534.36";
    
    // var _ua = "Mozilla/5.0 (X11; Linux amd64) AppleWebKit/534.36 (KHTML, like Gecko) Chrome/13.0.766.0 Safari/534.36";
    // var _ua = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5";



    // var _ua = "Mozilla/5.0 (X11; U; Linux x86_64; en-us) AppleWebKit/531.2+ (KHTML, like Gecko) Version/5.0 Safari/531.2+";
    // var _ua = "Mozilla/5.0 (Linux; U; Android 2.2.1; en-ca; LG-P505R Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";

    
    
    console.log('opts', opts);
    var options = {
        desiredCapabilities: {
            browserName: config.browserName,
            chromeOptions: {
                args: [
                    '--user-agent='+ _ua//user.useragent
                ]
                
            },
            // applicationCacheEnabled: true,
            databaseEnabled: true,
            acceptSslCerts:true,
            // browserConnectionEnabled: true,
            // webStorageEnabled: true,
            unexpectedAlertBehaviour: 'ignore',
            'phantomjs.cli.args': [
                '--ssl-protocol=any', 
                '--disk-cache=true',
                '--load-images=true',
                '--ignore-ssl-errors=true',
                // '--cookies-file='+config.root+'/cookies/'+user.id+'.txt'
            ],
            'phantomjs.page.settings.userAgent': _ua, //user.useragent,
            // 'chrome.page.settings.userAgent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36',// user.useragent,
            // 'phantomjs.page.settings.userAgent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36',// user.useragent,
            'phantomjs.page.settings.javascriptEnabled': true,
            'phantomjs.page.settings.loadImages': true,
            'phantomjs.page.settings.localToRemoteUrlAccessEnabled': false,
        },
      };
    if(user.proxy){
      options.desiredCapabilities.proxy = {
        proxyType: 'manual',
        httpProxy : user.proxy
      };
    }
	this.options = _.merge( options, opts || {} );

    //console.log('this.options', this.options);

    this.start = function(url){
        console.log('start');



        var client = webdriverio
            .remote(options)
            .init()
            .setViewportSize(config.viewportSize);
        // client.isMobile = true;

        var _on_err = function(err){
            client.end();
            throw err;
        };

        client.on('error', function(e) {
            // will be executed everytime an error occured
            // e.g. when element couldn't be found
            console.log('\n===========client error===========\n');
            console.log(e.body.value.class);   // -> "org.openqa.selenium.NoSuchElementException"
            console.log(e.body.value.message); // -> "no such element ..."
            console.log('\n==================================\n');
        });

        var screenService = ScreenService(client, user, db, config);
        screenService.clear();
        // s

        LoginService.login(client, user, db, config).then(function(){
            //todo перемешать сервисы рандомно
            console.log('on login');

            (new readNewsService(client, user, db, config)).run().then(function(){
                console.log('on read');
                client
                    .pause(3000)
                    .end();    
            }).catch(_on_err);


            
        }).catch(_on_err);
    }

}




module.exports = fn;