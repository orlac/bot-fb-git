'use strict';

/**
 * Время выполнения каждого действия, для каждого юзера случайно
 */

var Chance = require('chance');
var Promise = require('promise');
var chance = new Chance();

var actions = {
	ONLINE: 'ONLINE',
	//время просмотра френд ленты после промотки вниз
	READ_NEWS: 'READ_NEWS',
	//количество промоток вниз
	COUNT_READ_NEWS: 'READ_NEWS',
	COUNT_LINKS_NEWS: 'COUNT_LINKS_NEWS',
}

/**
 * [run description]
 * @param  {models.user} user   [description]
 * @param  {string} action [description]
 * @return {Promise}        [description]
 */
var run = function(user, action){
	

	//TODO брать настройки из базы и как-нибудь высчитывать
	return new Promise(function (resolve, reject) {

		switch(action){
			case actions.READ_NEWS:
				resolve(chance.integer({min: 2, max: 10}));
			break;
			case actions.COUNT_READ_NEWS:
				resolve(chance.integer({min: 2, max: 5}));
			break;
			default:
				resolve(chance.second());	
		}

		
	});
	
}

var getCountLinks = function(elements, user, action){
	return new Promise(function (resolve, reject) {
		resolve(5);	
	});	
}

var getRandomLink = function(elements, user, action){
	return new Promise(function (resolve, reject) {

		var res = chance.shuffle(elements).slice(0, 1).shift();

		// var keys = Object.keys(elements);
		// keys = chance.pik(keys, 5);

		// var res = [];

		// for(var i in keys){
		// 	res.push(elements[keys[i]]);			
		// }
		// res.shuffle(res);
		resolve(res);	

	});	
}

module.exports = {
	run: run,
	actions: actions,
	getCountLinks: getCountLinks,
	getRandomLink: getRandomLink
}