'use strict';

/**
 * Первый заход
 */

var Promise = require('promise');


var _fn = function(casper, user, db, config){

	this.db = db;
	this.config = config;
	this.user = user;
	this.casper = casper;

	var mainUrl = config.target.home;

	/*var setCookie = function(){
		var cookie = user.cookie;
		var domain = "facebook.com";
		cookie.split(";").forEach(function(pair){
		    pair = pair.split("=");
		    phantom.addCookie({
		      'name': pair[0],
		      'value': pair[1],
		      'domain': domain
		    });
		});
	}*/

	// this.init  = function(){
		
	// 	var casper = require('casper').create({
	// 		verbose: true,
	// 		// logLevel: 'error',
	// 		pageSettings: {
	// 			loadImages: true,
	// 			loadPlugins: true,
	// 			userAgent: user.useragent
	// 		},
	// 		viewportSize: {
	// 			width: 1366,
	// 			height: 768
	// 		}
	// 	});
	// 	this.casper = casper;
	// }

	this.login = function(){
		// return new Promise(function (resolve, reject) {
		// 	resolve(chance.second());
		// });
		console.log('mainUrl', mainUrl);
		casper.start(mainUrl);
		var _fn = function(){
			
			this.emit('dbg', 'start login');

			if (this.exists('#login_form')) {
				this.emit('dbg', 'login_form...');
		        this.fill('form#login_form', {
		        	email: user.email,
		        	pass: user.password,
		        }, true);
				// this.wait(3000);

				this.click('[type="submit"]');

				casper.waitForSelector('[title="Профиль"]', function(){
					this.echo ('logined')
					this.emit('dbg', 'logined...');
				});

		    }else{
		    	this.echo ('already logined')
		    	this.emit('dbg', 'already logined...');
		    }
		}
		casper.then([{user: user}, _fn]);
	}

	//this.init();

}


module.exports = function(casper, user, db, config){

	return new _fn(casper, user, db, config);

}