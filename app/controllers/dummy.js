var express = require('express'),
  router = express.Router(),
  //marko = require('marko'),
  db = require('../models');
var config = require('../../config/config.js');  

//var indexTemplate = marko.load(require.resolve('../views/index.marko'));

module.exports = function (app) {
  
  app.use(function (req, res, next) {
  	//console.log('config', config);
  		res.render('index', {
  			config: config,
  			title: '',
	      	date: new Date().toUTCString()
	    });
  });

};

