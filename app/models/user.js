
//var db = require('../models');


//var user_auth = sequelize.import(__dirname + "/user_auth");
var _ = require('lodash');
var async = require('async');

module.exports = function (sequelize, DataTypes) {


  var users = sequelize.define('user', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      email: {
        type: DataTypes.STRING,
        validate: {
            notEmpty:{
                msg: "Введите e-mail"
            }
        }
      },
      password: {
        type: DataTypes.STRING,
        validate: {
            notEmpty:{
                msg: "Введите пароль"
            }
        }
      },
      useragent: {
        type: DataTypes.TEXT,
        validate: {
            notEmpty:{
                msg: "Введите user-agent"
            }
        }
      },
      proxy: {
        type: DataTypes.STRING
      },
      proxy_type: {
        type: DataTypes.STRING
      },
      createdAt: {
        type: DataTypes.DATE
      },
      updatedAt: {
        type: DataTypes.DATE
      },
      active: { 
        type: DataTypes.BOOLEAN, 
        allowNull: false, 
        defaultValue: true
      },
      is_online: { 
        type: DataTypes.BOOLEAN, 
        allowNull: false, 
        defaultValue: false
      },
      cookie: {
        type: DataTypes.TEXT
      }
    
  }, {
    classMethods: {
      //createOrUpdateUser: _createOrUpdateUser,
      associate: function (models) {
        // example on how to add relations
        // Article.hasMany(models.Comments);
      }
    },

    instanceMethods: {

      setUserCookies: function(obj){
        this.cookie = JSON.stringify(obj);
      },

      getUserCookies: function(){
        return (this.cookie)? JSON.parse(this.cookie) : null;
      },
      // getServices: function(){
      //   return user_auth.findAll({where: {user_id: this.id}})
      // }
    }
  });

  return users;
};